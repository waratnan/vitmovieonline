//
//  ShelfViewController.m
//  VITMovie
//
//  Created by Waratnan Suriyasorn on 11/13/2556 BE.
//  Copyright (c) 2556 Waratnan Suriyasorn. All rights reserved.
//

#import "ShelfViewController.h"
#import "UIImageView+AFNetworking.h"
#import "AFJSONRequestOperation.h"
#import "XCDYouTubeVideoPlayerViewController.h"
@interface ShelfViewController (){
    NSArray *videoArray;
}

@end

@implementation ShelfViewController
@synthesize myTable;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self loadListVideo:_playListID];
}

- (void)loadListVideo:(NSString*)playlistID{
    
    NSString* urlString = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=%@&key=AIzaSyArQJ-AM4xM1Hx305oPouaFJSOZlnF6wxc",playlistID];
    
//    NSLog(@"CALL API : %@", urlString);
    
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
             NSDictionary *jsonDict = (NSDictionary *) JSON;

             videoArray = [jsonDict objectForKey:@"items"];
//             NSLog(@"%@",videoArray);
             [myTable reloadData];
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response,NSError *error, id JSON) {
             NSLog(@"Request Failure Because %@",[error userInfo]);
         }];
    [operation start];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [videoArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    cell.tag = indexPath.row;
    
        NSDictionary *dicRow = (NSDictionary*)[videoArray objectAtIndex:indexPath.row];
        NSDictionary *snippet = [dicRow objectForKey:@"snippet"];

        if(snippet!=nil){
            //set title
            NSDictionary *thumbnails = [snippet objectForKey:@"thumbnails"];
            NSDictionary *mediumImg = [thumbnails objectForKey:@"medium"];
            
            NSURL* urlImage = [NSURL URLWithString:[mediumImg objectForKey:@"url"]];
            NSString* strTitle = [snippet objectForKey:@"title"];
            
            UIImageView *cellImage = (UIImageView *)[cell viewWithTag:900];
            [cellImage setImageWithURL:urlImage];
            
            UILabel *txtName = (UILabel *)[cell viewWithTag:901];
//            [txtName sizeToFit];
            [txtName setText:strTitle];
            
        }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
    NSDictionary *dicRow = (NSDictionary*)[videoArray objectAtIndex:indexPath.row];
    NSDictionary *snippet = [dicRow objectForKey:@"snippet"];
    NSDictionary *resourceId = [snippet objectForKey:@"resourceId"];
    NSString *vcode = [resourceId objectForKey:@"videoId"];
    
    XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:vcode];
    [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
    
    CGRect masterRect = [[UIScreen mainScreen] bounds];
    [[videoPlayerViewController view] setBounds:CGRectMake(0, 0, masterRect.size.height, masterRect.size.width)];
    [[videoPlayerViewController view] setTransform:CGAffineTransformMakeRotation(M_PI_2)];

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
