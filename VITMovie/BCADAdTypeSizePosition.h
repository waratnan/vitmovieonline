//
//  BCADTypeAdSizePosition.h
//  BuzzCity iOS SDK 3.0
//
//  Created by Cjin Pheow Lee on 20/6/12.
//  Copyright (c) 2012 BuzzCity Pte Ltd. All rights reserved.
//

#pragma mark supported Ad types

/** Ad types **/
typedef enum {
    BCAD_TYPE_IMAGE,
    BCAD_TYPE_TEXT
} BCADAdType;


#pragma mark supported Banner Sizes

/** Ad sizes **/
typedef enum {
    BCAD_SIZE_320x50,
    BCAD_SIZE_CUSTOM
} BCADAdSize;


#pragma mark Banner Positions

/** Ad positions **/
typedef enum {
    BCAD_POSITION_TOPLEFT,
    BCAD_POSITION_TOP,        // top center
    BCAD_POSITION_TOPRIGHT,
    BCAD_POSITION_BOTTOMLEFT,
    BCAD_POSITION_BOTTOM,     // bottom center
    BCAD_POSITION_BOTTOMRIGHT,
    BCAD_POSITION_CUSTOM      // custom position requires origin 
} BCADAdPosition;

