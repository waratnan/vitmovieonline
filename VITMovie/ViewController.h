//
//  ViewController.h
//  VITMovie
//
//  Created by Waratnan Suriyasorn on 11/13/2556 BE.
//  Copyright (c) 2556 Waratnan Suriyasorn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCADAdView.h"

@interface ViewController : UIViewController <BCADAdViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *adsView;
@property (nonatomic, strong) BCADAdView *adView;

@end
