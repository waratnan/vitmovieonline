//
//  AppDelegate.m
//  VITMovie
//
//  Created by Waratnan Suriyasorn on 11/13/2556 BE.
//  Copyright (c) 2556 Waratnan Suriyasorn. All rights reserved.
//

#import "AppDelegate.h"
#import "GADInterstitial.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    GADInterstitial *splashInterstitial_ = [[GADInterstitial alloc] init];
    splashInterstitial_.adUnitID = INTERNITIALS_ADS_ID;
    splashInterstitial_.delegate = self;
    
    [splashInterstitial_ loadAndDisplayRequest:[GADRequest request]
                                   usingWindow:self.window
                                  initialImage:nil];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (void)interstitialDidReceiveAd:(GADInterstitial *)interstitial {
    NSLog(@"RECEIVED AD INTERNITIAL ADS");
}
-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error{
    NSLog(@"Failed to RECEIVED AD INTERNITIAL ADS : %@", [error localizedFailureReason]);
    
}

@end
