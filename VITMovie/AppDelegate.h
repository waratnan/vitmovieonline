//
//  AppDelegate.h
//  VITMovie
//
//  Created by Waratnan Suriyasorn on 11/13/2556 BE.
//  Copyright (c) 2556 Waratnan Suriyasorn. All rights reserved.
//
#define INTERNITIALS_ADS_ID @"eca667d399134d64"

#import <UIKit/UIKit.h>
#import "GADInterstitial.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,GADInterstitialDelegate>

@property (strong, nonatomic) UIWindow *window;

@end