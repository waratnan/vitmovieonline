//
//  ViewController.m
//  VITMovie
//
//  Created by Waratnan Suriyasorn on 11/13/2556 BE.
//  Copyright (c) 2556 Waratnan Suriyasorn. All rights reserved.
//

#define MY_BUZZCITY_PARTNER_ID 107008//97285                // set your BuzzCity partner id as a constant here

#import "ViewController.h"
#import "ShelfViewController.h"
#import "BCADAdView.h"

@interface ViewController (){

}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.adView = [[BCADAdView alloc] initWithType:BCAD_TYPE_IMAGE  //BCAD_TYPE_TEXT for text ads
                                              size:BCAD_SIZE_320x50
                                          position:BCAD_POSITION_BOTTOM
                                            origin:CGPointZero];
    
    // Specify your BuzzCity Partner ID
    self.adView.partnerID = MY_BUZZCITY_PARTNER_ID;
    
    // Add the adView as a subview to the controller's view hierarchy
    [self.adsView addSubview:self.adView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    ShelfViewController *vc = [segue destinationViewController];
    vc.title =[[sender titleLabel]text];
    NSString *playlistId;
    if ([segue.identifier isEqualToString:@"thai"]) {
        playlistId = @"PLV2RPRKu4OKw3X2uBUIHGbfyOzXnv6V4p";
    }else if([segue.identifier isEqualToString:@"inter"]) {
        playlistId = @"PLV2RPRKu4OKzIPmTIW5eUAhm3h9q0bt_9";

    }else if([segue.identifier isEqualToString:@"asian"]) {
        playlistId = @"PLV2RPRKu4OKwn4ssSpVJ-4k_egARR-MVA";

    }else if([segue.identifier isEqualToString:@"cartoon"]) {
        playlistId = @"PLV2RPRKu4OKwBpsz8j6b0svXwtrgB1aMy";

    }else {
        playlistId = @"PLV2RPRKu4OKzyo-cn53J0tWU3Ke04iMPs";
    }
    [vc setPlayListID:playlistId];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.adView = nil;
}

@end
