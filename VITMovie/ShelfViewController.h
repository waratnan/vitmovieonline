//
//  ShelfViewController.h
//  VITMovie
//
//  Created by Waratnan Suriyasorn on 11/13/2556 BE.
//  Copyright (c) 2556 Waratnan Suriyasorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShelfViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITableView *myTable;
@property (retain,nonatomic) NSString *playListID;
@end
