//
//  BCADAdView.h
//  BuzzCity iOS SDK 3.0
//
//  Created by Cjin Pheow Lee on 17/7/12.
//  Copyright (c) 2012 BuzzCity Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCADAdTypeSizePosition.h"
#import "BCADAdViewDelegate.h"
#import "BCADRequest.h"

@interface BCADAdView : UIView

#pragma mark Designated Initializer

/** Designated Initializer

    @param         (BCADAdType)type: Type of Ad (default BCAD_TYPE_IMAGE)
    @param         (BCADAdSize)size: Size of Ad (default BCAD_SIZE_320x50)
    @param (BCADAdPosition)position: Position of Ad (default BCAD_POSITION_BOTTOM)
           if position is BCAD_POSITION_CUSTOM, then the following "origin" is used
    @param          (CGPoint)origin: Origin (top-left coord) of Ad (ONLY when position is BCAD_POSITION_CUSTOM)
 */

- (id)initWithType:(BCADAdType)type 
              size:(BCADAdSize)size
          position:(BCADAdPosition)position
            origin:(CGPoint)origin;


#pragma mark Pre-Request (REQUIRED)

/** Your BuzzCity partner id. Register at http://www.buzzcity.com if you don't have one.
    NOTE: You MUST set this property before you fetch any ads
 */

@property (nonatomic, assign) unsigned long partnerID;

#pragma mark Pre-Request (OPTIONAL)

/** Your BuzzCity application id.
 NOTE: You MUST set this property in order to track installations of your app
 */

@property (nonatomic, strong) NSString *applicationID;

/** Custom Ad Size.
 For example, "320x50", "300x50"
 */

@property (nonatomic, strong) NSString *customAdSize;

/** Number of seconds for your ad to fully appear.
 Default is 0, ie, immediately upon fetched.
 */

@property (nonatomic, assign) NSTimeInterval adAppearingDuration;

/** Set the content offset to prevent ad from being moved when the view scrolls.
    You should set this in the "- (void)scrollViewDidScroll:(UIScrollView *)scrollView" delegate method.
    You should optionally set tableView.contentInset and tableView.scrollIndicatorInsets to appropriate
    values so that your ad will not overlap your content when scrolled to the edges.
 */

@property (nonatomic, assign) CGPoint contentOffset;

/** Automatically request for an ad
    This basically calls the "loadRequest" method with a default BCADRequest on your behalf when
    the ad view is visible on screen. Subsequent ad refreshing is still subject to autorefresh value 
    of BCADRequest (which is 0s by default)
    Default is YES.
 */

@property (nonatomic, assign) BOOL autoLoadRequest;

/** Show the top ad below the status bar instead of behind the status bar.
    This only affects the ad display in iOS 7 or later for BCAD_POSITION_TOP, BCAD_POSITION_TOPLEFT and BCAD_POSITION_TOPRIGHT positions.
    Default is NO.
 */

@property (nonatomic, assign) BOOL showAdBelowStatusBar;

/** Delegate object that implements the BCADAdViewDelegate protocol methods to receive 
    notifications of ad loading and interaction status
 */

@property (nonatomic, assign) NSObject <BCADAdViewDelegate> *delegate;


#pragma mark Manually make an Ad Request

/** loadRequest
    @param (BCADRequest *)request: the request object
    Makes an ad request manually. Additional targeting options can be supplied with a
    request object. The ad will auto refresh according to the autorefresh value
    of the BCADRequest object you passed to this method. To manually refresh ads 
    (ie, to disable auto ads refreshing), set BCADRequest.autorefresh to 0.
 */

- (void)loadRequest:(BCADRequest *)request;

@end
