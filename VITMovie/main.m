//
//  main.m
//  VITMovie
//
//  Created by Waratnan Suriyasorn on 11/13/2556 BE.
//  Copyright (c) 2556 Waratnan Suriyasorn. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
