//
//  BCADRequest.h
//  BuzzCity iOS SDK 3.0
//
//  Created by Cjin Pheow Lee on 20/6/12.
//  Copyright (c) 2012 BuzzCity Pte Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCADRequest : NSObject

#pragma mark Testing mode

/**
   Setting this property to YES will return a test ad for this request.
   Default is NO.
 */

@property (nonatomic, getter=isTesting) BOOL testing;


#pragma mark Ad Auto-refresh

/**
   Ad auto refresh interval - Setting to 0 means the ad never auto refresh.
   Default is 0. Shortest autorefresh period is set in bcads.js, normally 30s.
 */

@property (nonatomic, assign) int autorefresh;


#pragma mark Special Targetting details

/**
   Specific targetting instructions - currently not in use.
 */

@property (nonatomic, retain) NSDictionary *targets;

@end
