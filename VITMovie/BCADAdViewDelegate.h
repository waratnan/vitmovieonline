//
//  BCADAdViewDelegate.h
//  BuzzCity iOS SDK 3.0
//
//  Created by Cjin Pheow Lee on 16/7/12.
//  Copyright (c) 2012 BuzzCity Pte Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCADAdView;

@protocol BCADAdViewDelegate <NSObject>
@optional

/** will load ad **/
- (void)BCADAdViewWillLoadAd:(BCADAdView *)adView;

/** ad loaded **/
- (void)BCADAdViewDidLoadAd:(BCADAdView *)adView;

/** failed to receive ad **/
- (void)BCADAdViewDidFailToReceiveAd:(BCADAdView *)adView withError:(NSError *)error;

/** ad return control to your application **/
- (void)BCADAdViewActionDidFinish:(BCADAdView *)adView;

/** ad clicked occurred, will leave application **/
- (void)BCADAdViewWillLeaveApplication:(BCADAdView *)adView;

@end
